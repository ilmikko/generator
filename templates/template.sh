#!/usr/bin/env bash
# A one line comment what the program does.
# 
# Some other comments.
# Another line.
# 

if [ $# = 0 ]; then
	echo "Usage: $0 [...]";
	exit 1;
fi

while [ $# -gt 0 ]; do
	case $1 in
		--*)
			echo "Unknown command: $1";
			exit 1;
			;;
		*)
			;;
	esac
	shift;
done
