test_file="$%";
source_file=${test_file%.h}.c;
[ -f "$source_file" ] || exit 0;
cat $source_file | awk '/^\S*\s*\S*\(.*\)\s*{/ { sub(/\s{/,";",$0); print($0,"\n"); }';
