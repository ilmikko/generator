test_file="$%";
file=${test_file%.test};
[ -f "$file" ] || exit 0;
ext=${file##*.};
case $ext in
	sh)
		cat $file | awk '/^.*\s*\(.*\)\s*{/ { print("test_"$0); print("\texit 1;"); print("}") }';
		;;
esac
