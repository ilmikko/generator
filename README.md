# Generator

This is a helper for file templating.
In order to generate output, you can simply run `generator [filename]`.
Generator never replaces any files on its own; if you wanted to create a template file you would have to run `generator [filename] > [filename]`.

## Rules

Generator tries to guess which type of file to generate from the filename of such file.
For example, when running `generator filename.java`, some Java template code is created.
Generator also supports more complex rules - for example `generator file.go` and `generator file_test.go` will produce a different output.
Rules can be added into `./templates`. A rule with a .sh ending will be executed and the output of that script will be outputted as the template.
