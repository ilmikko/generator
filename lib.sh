log() {
	echo $@ 1>&2;
}

# Get a type for a file.
get_type() {
	file=$(basename "$1");
	if [ -f "$templates$file" ] || [ -f "$templates$file.sh" ]; then
		echo "$file";
	elif echo "$file" | grep -q "\."; then
		echo "$file" | sed -r 's/[A-Za-z0-9]+//';
	else
		echo "*";
	fi
}

update() {
	for file in $@; do
		echo "$file";
	done
}

sedsafe() {
	echo $@ | sed -e 's/\//\\\//g';
}

convfile() {
	f="$(sedsafe $(basename $@))";
	sed -e 's/$%/'"$f"'/g';
}

template() {
	file="$1";
	templates="$DIR/templates/template$type";
	type=$(get_type "$1");
	template_file="$templates$type";
	if [ -f "$template_file" ]; then
		template="$(cat "$template_file")";
		while read command; do
			output=$($command);
			template="$(echo "$template" | convfile $file | sed -e 's/{{\s*'"$command"'\s*}}/'"$(sedsafe $output)"'/')";
		done < <(cat "$template_file" | grep -o '{{.*}}' | sed -e 's/\(^{{\|}}$\)//g' | convfile $file)
		echo "$template";
	elif [ -f "$template_file.sh" ]; then
		cat $template_file.sh | sed -e 's/$%/'"$(sedsafe $file)"'/g' | bash;
	fi
}
